Sarah Desktop Partner
=====================

Android Client
--------------
[![License](https://img.shields.io/badge/License-GPL%20v3%2B-blue.svg?style=flat-square)](https://gitlab.com/sarah-desktop-partner/sarah-android-client/blob/master/LICENSE)

Android Client for [Sarah Desktop Partner Server Service](https://gitlab.com/sarah-desktop-partner/sarah-server-service) which is written in Java and it would enables you to operate your Android Smart Phone without touching it!

The more lazy you become, The more creative you will be :)