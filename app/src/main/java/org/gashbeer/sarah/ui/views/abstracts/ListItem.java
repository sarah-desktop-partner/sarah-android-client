
package org.gashbeer.sarah.ui.views.abstracts;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ebrahim on 6/30/19 for project bitkeeper.
 */
public abstract class ListItem {

    public abstract int getLayout();

    public abstract void onBind(RecyclerView.ViewHolder viewHolder);

    public abstract @NonNull
    SmartVH getViewHolder(@NonNull ViewGroup parent, int viewType);

}
