/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import org.gashbeer.sarah.connectivity.ConnectionService;
import org.gashbeer.sarah.Device;
import org.gashbeer.sarah.DeviceManager;
import org.gashbeer.sarah.R;
import org.gashbeer.sarah.SarahNotification;
import org.gashbeer.sarah.databinding.ActivityMainBinding;
import org.gashbeer.sarah.ui.views.DeviceConnectionSmartListItem;
import org.gashbeer.sarah.ui.views.DeviceSmartListItem;
import org.gashbeer.sarah.ui.views.abstracts.ListItemEvents;
import org.gashbeer.sarah.ui.views.abstracts.SmartListItem;
import org.gashbeer.sarah.ui.views.abstracts.SmartRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements ServiceConnection {

    private static final String TAG = "MainActivity";
    private ConnectionService.ConnectionBinder connectionBinder;
    private SmartRecyclerViewAdapter<SmartListItem> deviceSmartListItemSmartRecyclerViewAdapter;
    private List<SmartListItem> deviceSmartListItemList;
    private boolean isServiceBounded = false;
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeStatusBarColorTransparent(this);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        beginService();
        initView();
        setContentView(binding.getRoot());
    }

    /*
    For making status bar transparent
     */
    public static void makeStatusBarColorTransparent(Activity activity) {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    private void beginService(){
        Intent intent = new Intent(getApplicationContext(), ConnectionService.class);
        startService(intent);
    }

    private void initView(){
        deviceSmartListItemList = new ArrayList<>();
        deviceSmartListItemSmartRecyclerViewAdapter = new SmartRecyclerViewAdapter<>(deviceSmartListItemList);
        binding.rcvDevicesList.setLayoutManager(new LinearLayoutManager(this));
        binding.rcvDevicesList.setAdapter(deviceSmartListItemSmartRecyclerViewAdapter);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        connectionBinder = (ConnectionService.ConnectionBinder) service;
        connectionBinder.addStatusChangedListener(update);
        updateUI();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        connectionBinder = null;
    }


    ListItemEvents<SmartListItem<Device>> deviceConnectionItemEvents = new ListItemEvents<SmartListItem<Device>>() {
        @Override
        public void onClick(View v, SmartListItem<Device> object) {
            Log.e(TAG, "onClick: " + object.getT().getName());
        }

        @Override
        public void onMore(View v, SmartListItem<Device> object) {
            Log.e(TAG, "onMore: " + object.getT().getName());
            Intent intent = new Intent(MainActivity.this, DeviceSettingsActivity.class);
            startActivity(intent);

        }

        @Override
        public void onDelete(View item, SmartListItem<Device> object) {
            Log.e(TAG, "onDelete: " + object.getT().getName());

        }
    };


    ListItemEvents<SmartListItem<Device>> deviceItemEvents = new ListItemEvents<SmartListItem<Device>>() {
        @Override
        public void onClick(View v, SmartListItem<Device> object) {

        }

        @Override
        public void onMore(View v, SmartListItem<Device> object) {
            Intent acceptIntent = new Intent(MainActivity.this, ConnectionService.class);
            acceptIntent.setAction(SarahNotification.Channels.Connection.name);
            acceptIntent.putExtra("id", object.getT().getId());
            acceptIntent.putExtra("accept", true);
            startService(acceptIntent);
        }

        @Override
        public void onDelete(View item, SmartListItem<Device> object) {

        }
    };


    DeviceManager.ManagementUpdate update = new DeviceManager.ManagementUpdate() {
        @Override
        public void onStatusChanged() {
            updateUI();

        }

        @Override
        public void onDiscoveredDevice(Device device) {
            updateUI();
        }
    };

    void updateUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connectionBinder != null) {
                    deviceSmartListItemList.clear();
                    for (Device dCon :
                            connectionBinder.getDeviceManager().getDiscoveredDevices()) {
                        SmartListItem deviceSmartListItem;
                        if (connectionBinder.getDeviceManager().getConFromID(dCon.getId()) != null) {
                            deviceSmartListItem = new DeviceConnectionSmartListItem(dCon, deviceConnectionItemEvents);
                        } else {
                            deviceSmartListItem = new DeviceSmartListItem(dCon, deviceItemEvents);
                        }
                        deviceSmartListItemList.add(deviceSmartListItem);
                    }
                    if (deviceSmartListItemList.size()>0)
                        showDeviceList();
                    deviceSmartListItemSmartRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void showDeviceList(){
        binding.rcvDevicesList.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        binding.textViewLookingForDevice.setVisibility(View.GONE);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (isServiceBounded)
            unbindService(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(getApplicationContext(), ConnectionService.class);
        if (bindService(intent, this, BIND_AUTO_CREATE)) {
            isServiceBounded = true;
        }
    }
}
