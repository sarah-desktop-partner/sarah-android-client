package org.gashbeer.sarah.ui.views.abstracts;

import android.view.View;

/**
 * Created by ebrahim on 6/28/19 for project bitkeeper.
 */
public interface ListItemEvents<T> {

    void onClick(View v, T object);

    void onMore(View v, T object);

    void onDelete(View item, T object);
}
