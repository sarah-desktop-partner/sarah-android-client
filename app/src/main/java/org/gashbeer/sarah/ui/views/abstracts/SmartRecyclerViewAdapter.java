package org.gashbeer.sarah.ui.views.abstracts;

import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ebrahim on 6/28/19 for project bitkeeper.
 */
public class SmartRecyclerViewAdapter<T extends SmartListItem> extends RecyclerView.Adapter {
    private List<T> itemList;

    public SmartRecyclerViewAdapter(List<T> items) {
        this.itemList = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        for (T item :
                itemList) {
            if (item.getLayout() == viewType)
                return item.getViewHolder(parent, viewType);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        itemList.get(position).onBind(holder);
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getLayout();
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private static List<Object> list = new ArrayList<>();

    public List<Object> getList() {
        for (T item :
                itemList) {
            list.add(item.getT());
        }
        return list;
    }

    public Object getTitem(int index) {
        return itemList.get(index).getT();
    }

}
