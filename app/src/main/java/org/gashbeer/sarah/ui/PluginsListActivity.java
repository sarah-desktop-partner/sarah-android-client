package org.gashbeer.sarah.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import org.gashbeer.sarah.R;

public class PluginsListActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.makeStatusBarColorTransparent(this);
        setContentView(R.layout.activity_plugins_list);
    }
}