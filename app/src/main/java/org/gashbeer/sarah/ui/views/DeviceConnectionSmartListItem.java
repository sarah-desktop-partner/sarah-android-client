/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.ui.views;

import android.widget.Button;
import android.widget.TextView;

import org.gashbeer.sarah.Device;
import org.gashbeer.sarah.R;
import org.gashbeer.sarah.ui.views.abstracts.ListItemEvents;
import org.gashbeer.sarah.ui.views.abstracts.SmartListItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ebrahim on 11/28/19 for project Sarah.
 */
public final class DeviceConnectionSmartListItem extends DeviceSmartListItem {
    public DeviceConnectionSmartListItem(@Nullable ListItemEvents<SmartListItem<Device>> events) {
        super(events);
    }

    public DeviceConnectionSmartListItem(@NonNull Device object) {
        super(object);
    }

    public DeviceConnectionSmartListItem(@NonNull Device object, @Nullable ListItemEvents<SmartListItem<Device>> events) {
        super(object, events);
    }

    @Override
    public void onBind(RecyclerView.ViewHolder viewHolder) {
        super.onBind(viewHolder);
        TextView title = viewHolder.itemView.findViewById(R.id.txtview_title);
        title.append(" (" + viewHolder.itemView.getContext().getString(R.string.connected) + ")");
        Button btn_action = viewHolder.itemView.findViewById(R.id.btn_action);
        btn_action.setText(R.string.settings);
    }
}
