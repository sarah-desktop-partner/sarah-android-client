package org.gashbeer.sarah.ui.views.abstracts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by ebrahim on 6/30/19 for project bitkeeper.
 */
public abstract class SmartListItem<T> extends ListItem implements ListItemEvents<SmartListItem<T>>, View.OnClickListener {
    protected ListItemEvents<SmartListItem<T>> events;
    protected T t;
    private Context context;

    public SmartListItem(@Nullable ListItemEvents<SmartListItem<T>> events) {
        this.events = events;
    }

    public SmartListItem(@NonNull T object) {
        this.events = null;
        this.t = object;
    }

    public SmartListItem(T object, @Nullable ListItemEvents<SmartListItem<T>> events) {
        this(events);
        this.t = object;
    }


    @Override
    public void onClick(View v, SmartListItem<T> object) {
        if (events != null)
            events.onClick(v, object);
    }

    @Override
    public void onDelete(View item, SmartListItem<T> object) {
        if (events != null)
            events.onDelete(item, object);
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public void onClick(View v) {
        if (events != null)
            events.onClick(v, this);
    }

    @Override
    public void onMore(View v, SmartListItem<T> object) {
        if (events != null)
            events.onMore(v, this);
    }

    @NonNull
    @Override
    public SmartVH getViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(getLayout(), parent, false);
        context = itemView.getContext();
        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        return new SmartVH(itemView);
    }

    public Context getContext() {
        return context;
    }
}
