/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.ui.views;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.gashbeer.sarah.Device;
import org.gashbeer.sarah.R;
import org.gashbeer.sarah.ui.views.abstracts.ListItemEvents;
import org.gashbeer.sarah.ui.views.abstracts.SmartListItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ebrahim on 11/28/19 for project Sarah.
 */
public class DeviceSmartListItem extends SmartListItem<Device> {
    public DeviceSmartListItem(@Nullable ListItemEvents<SmartListItem<Device>> events) {
        super(events);
    }

    public DeviceSmartListItem(@NonNull Device object) {
        super(object);
    }

    public DeviceSmartListItem(@NonNull Device object, @Nullable ListItemEvents<SmartListItem<Device>> events) {
        super(object, events);
    }

    @Override
    public int getLayout() {
        return R.layout.device_row_layout;
    }

    @Override
    public void onBind(RecyclerView.ViewHolder viewHolder) {
        ((TextView) viewHolder.itemView.findViewById(R.id.txtview_title)).setText(getT().getName());
        ((TextView) viewHolder.itemView.findViewById(R.id.txtview_details)).setText(getT().getAddress().getHostAddress());
        if (getT().getOs() != null) {
            if (getT().getOs().toLowerCase().contains("windows")) {
                ((ImageView) viewHolder.itemView.findViewById(R.id.img_avatar)).setImageResource(R.drawable.img_windows_logo);
            } else {
                ((ImageView) viewHolder.itemView.findViewById(R.id.img_avatar)).setImageResource(R.drawable.img_linux_logo);
            }
        } else {
            ((ImageView) viewHolder.itemView.findViewById(R.id.img_avatar)).setImageResource(R.drawable.logo_foreground_black_large);
        }
        viewHolder.itemView.findViewById(R.id.device_row_root).setOnClickListener(this);
        Button btn_action = viewHolder.itemView.findViewById(R.id.btn_action);
        btn_action.setText(R.string.connect);
        btn_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMore(v, DeviceSmartListItem.this);
            }
        });
    }
}
