package org.gashbeer.sarah.ui.views.abstracts;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ebrahim on 6/30/19 for project bitkeeper.
 */
class SmartVH extends RecyclerView.ViewHolder {
    SmartVH(@NonNull View itemView) {
        super(itemView);
    }
}
