/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;

import androidx.annotation.NonNull;

/**
 * Created by ebrahim on 11/18/19 for project Sarah.
 */
public class SarahPacket {
    protected JSONObject jsonObject;
    protected boolean hasError = false;
    protected InetAddress senderAddress;

    public SarahPacket(String data, InetAddress senderAddress) {
        this.senderAddress = senderAddress;
        try {
            jsonObject = new JSONObject(data);
        } catch (JSONException ex) {
            hasError = true;
        }
    }

    public SarahPacket(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public boolean hasType() {
        return getJsonObject().has("type");
    }

    public String getType() {
        try {
            return jsonObject.getString("type");
        } catch (Exception e) {
            return "Unknown";
        }
    }

    public boolean hasError() {
        return hasError;
    }

    public InetAddress getSenderAddress() {
        return senderAddress;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    @NonNull
    @Override
    public String toString() {
        return jsonObject.toString();
    }
}
