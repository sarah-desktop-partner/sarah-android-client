/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah;

import android.os.Looper;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

import androidx.annotation.Nullable;

/**
 * Created by ebrahim on 11/19/19 for project Sarah.
 */
public class SarahSocket extends Socket {

    public interface SocketStateChanged {
        void onSocketConnected(SarahSocket socket);

        void onSocketDisconnected(SarahSocket socket);
    }

    protected SocketStateChanged[] socketStateChanged;

    public SarahSocket(InetAddress address, int port, SocketStateChanged... socketStateChangeds) throws IOException {
        super(address, port);
        this.socketStateChanged = socketStateChangeds;
        for (SocketStateChanged socketStateChanged :
                socketStateChangeds) {
            socketStateChanged.onSocketConnected(this);
        }
    }

    public int receivePacket(byte[] buffer) throws IOException {
        int tmp = -1;
        try {
            tmp = getInputStream().read(buffer);
        } catch (SocketException e) {
            for (SocketStateChanged socketStateChanged :
                    this.socketStateChanged) {
                socketStateChanged.onSocketDisconnected(this);
            }
        }
        Log.e(TAG, "receivePacket: " + new String(buffer));
        if (tmp == -1)
            for (SocketStateChanged socketStateChanged :
                    this.socketStateChanged) {
                socketStateChanged.onSocketDisconnected(this);
            }
        return tmp;
    }

    private static final String TAG = "SarahSocket";

    public void sendPacket(byte[] packet) throws IOException {
        Log.e(TAG, "sendPacket: " + Looper.getMainLooper().getThread().getName());
        getOutputStream().write(packet);
    }

    public void sendPacket(String packet) throws IOException {
        Log.e(TAG, "sendPacket: " + Looper.getMainLooper().getThread().getName());
        getOutputStream().write(packet.getBytes());
    }

    public void sendPacket(SarahPacket packet) throws IOException {
        Log.e(TAG, "sendPacket: " + Looper.getMainLooper().getThread().getName());
        getOutputStream().write(packet.toString().getBytes());
    }

    public void sendPacket(JSONObject packet) throws IOException {
        Log.e(TAG, "sendPacket: " + Looper.getMainLooper().getThread().getName());

        getOutputStream().write(packet.toString().getBytes());
    }


    public boolean equals(@Nullable SarahSocket obj) {
        return obj != null && getInetAddress().equals(obj.getInetAddress()) && getPort() == obj.getPort();
    }
}
