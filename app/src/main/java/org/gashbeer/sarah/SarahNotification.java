/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/


package org.gashbeer.sarah;

import android.app.Notification;
import android.app.RemoteInput;
import android.content.Context;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;

import org.json.JSONArray;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;

/**
 * Created by ebrahim on 11/13/19 for project Sarah.
 */
public class SarahNotification {
    private static final String TAG = "SarahNotification";

    public static class Channels {
        public static class Persistent {
            public static int id = 3519;
            public static String name = "Persistent";

        }

        public static class Authentication {
            public static int id = 2599;
            public static String name = "Authentication";
            public static String intent_key = "key_code";

        }

        public static class Connection {
            public static int id = 35192599;
            public static String name = "Connection";

        }
    }

    public static void dismissNotification(Context context, int id) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancel(id);
    }

    private StatusBarNotification statusBarNotification;

    public SarahNotification(StatusBarNotification statusBarNotification) {
        this.statusBarNotification = statusBarNotification;
    }

    /**
     * Self-Serializing to JSON
     *
     * @param context in case we want a better information otherwise pass `null` its recommended to be provided
     * @return serialized {@link SarahNotification} to {@link JSONObject} (json format)
     */
    public JSONObject serializeToJSON(@Nullable Context context) {
        JSONObject object = new JSONObject();
        try {
            String appName = statusBarNotification.getPackageName();
            if (context != null) {
                appName = SarahPackageManagerHelper.appNameLookup(context, appName);
            }
            object.put("notificationID", statusBarNotification.getId());
            object.put("appID", statusBarNotification.getPackageName());
            object.put("appName", appName == null ? statusBarNotification.getPackageName() : appName);
            String title = statusBarNotification.getNotification().extras.getString("android.title");
            String text = statusBarNotification.getNotification().extras.getString("android.text");
            object.put("title", title);
            object.put("description", text);
            object.put("actions", serializeActions());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    public JSONArray serializeActions() {
        JSONArray actions = new JSONArray();
        for (Notification.Action action :
                statusBarNotification.getNotification().actions) {
            try {
                JSONObject actionJsonObj = new JSONObject();
                actionJsonObj.put("title", action.title);
                JSONArray remoteInputsArray = new JSONArray();
                if (action.getRemoteInputs() != null)
                    for (RemoteInput remoteInput :
                            action.getRemoteInputs()) {
                        JSONObject remoteInputObj = new JSONObject();
                        remoteInputObj.put("title", remoteInput.getLabel());
                        remoteInputObj.put("replyKey", remoteInput.getResultKey());
                        remoteInputObj.put("extras", bundleExtrasToJson(remoteInput.getExtras()));
                        remoteInputsArray.put(remoteInputObj);
                    }
                actionJsonObj.put("remoteInputs", remoteInputsArray);
                actions.put(actionJsonObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return actions;
    }

    public static JSONObject bundleExtrasToJson(Bundle bundle) {
        JSONObject object = new JSONObject();
        for (String obj :
                bundle.keySet()) {
            try {
                object.put(obj, bundle.get(obj));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return object;
    }

}
