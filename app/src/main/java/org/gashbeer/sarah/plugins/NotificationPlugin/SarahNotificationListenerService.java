/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.plugins.NotificationPlugin;

import android.content.Context;
import android.content.Intent;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.annotation.RequiresApi;

import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;

/**
 * Created by ebrahim on 11/21/19 for project Sarah.
 */
@RequiresApi(api = JELLY_BEAN_MR2)
public class SarahNotificationListenerService extends NotificationListenerService {


    private final ArrayList<NotificationListener> listeners = new ArrayList<>();

    private boolean connected;


    public interface NotificationListener {
        void onNotificationPosted(StatusBarNotification statusBarNotification);

        void onNotificationRemoved(StatusBarNotification statusBarNotification);

        void onListenerConnected(SarahNotificationListenerService service);

        void onListenerDisconnected(SarahNotificationListenerService service);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        //Log.e("NotificationReceiver.onNotificationPosted","listeners: " + listeners.size());
        for (NotificationListener listener : listeners) {
            listener.onNotificationPosted(statusBarNotification);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
        for (NotificationListener listener : listeners) {
            listener.onNotificationRemoved(statusBarNotification);
        }
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        for (NotificationListener listener : listeners) {
            listener.onListenerConnected(this);
        }
        connected = true;
    }

    @Override
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        connected = false;
        for (NotificationListener listener : listeners) {
            listener.onListenerDisconnected(this);
        }
    }

    //To use the service from the outer (name)space

    public interface InstanceCallback {
        void onServiceStart(SarahNotificationListenerService service);
    }

    final static Lock listenersMutex = new ReentrantLock();

    public void addListener(NotificationListener listener) {
        listeners.add(listener);
    }

    public void removeListener(NotificationListener listener) {
        listeners.remove(listener);
    }

    private final static ArrayList<InstanceCallback> callbacks = new ArrayList<>();


    public static void Start(Context c) {
        RunCommand(c, null);
    }

    public static void RunCommand(Context c, final InstanceCallback callback) {
        if (callback != null) {
            listenersMutex.lock();
            try {
                callbacks.add(callback);
            } finally {
                listenersMutex.unlock();
            }
        }
        Intent serviceIntent = new Intent(c, SarahNotificationListenerService.class);
        c.startService(serviceIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        listenersMutex.lock();
        try {
            for (InstanceCallback c : callbacks) {
                c.onServiceStart(this);
            }
            callbacks.clear();
        } finally {
            listenersMutex.unlock();
        }
        return START_STICKY;
    }
}
