/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.plugins;

import android.content.Context;

import org.gashbeer.sarah.connectivity.DeviceConnection;
import org.gashbeer.sarah.plugins.ClipboardPlugin.ClipboardPlugin;
import org.gashbeer.sarah.plugins.NotificationPlugin.NotificationPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ebrahim on 11/20/19 for project Sarah.
 */
public final class PluginFactory {
    private static Map<Integer, Class<? extends Plugin>> availablePlugins = new ConcurrentHashMap<>();

    public static void initPluginFactory() {
        Plugin p = new ClipboardPlugin();
        availablePlugins.put(p.getPluginID(), p.getClass());
        p = new NotificationPlugin();
        availablePlugins.put(p.getPluginID(), p.getClass());

    }

    public static Plugin initiatePluginForDeviceConnection(Context context, DeviceConnection connection, int pluginKey) {
        try {
            if (availablePlugins.get(pluginKey) != null) {
                Plugin p = Objects.requireNonNull(availablePlugins.get(pluginKey)).newInstance();
                p.initPlugin(context, connection);
                return p;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Plugin> initiatePluginsForDeviceConnection(Context context, DeviceConnection connection) {
        List<Plugin> plugins = new ArrayList<>();
        try {
            for (Integer ClassID :
                    availablePlugins.keySet()) {
                if (availablePlugins.get(ClassID) != null) {
                    Plugin p = availablePlugins.get(ClassID).newInstance();
                    p.initPlugin(context, connection);
                    plugins.add(p);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return plugins;
    }

    public static Set<Integer> getAvailablePlugins() {
        return availablePlugins.keySet();
    }
}
