/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.plugins.NotificationPlugin;

import android.app.Activity;
import android.app.Notification;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;

import org.gashbeer.sarah.R;
import org.gashbeer.sarah.SarahNotification;
import org.gashbeer.sarah.SarahPacket;
import org.gashbeer.sarah.plugins.Plugin;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.core.app.NotificationCompat;

/**
 * Created by ebrahim on 11/21/19 for project Sarah.
 */
public class NotificationPlugin extends Plugin implements SarahNotificationListenerService.NotificationListener {

    public static String NOTIFICATION_PLUGIN_TYPE = "org.gashbeer.sarah.plugins.notification";
    private static final String TAG = "NotificationPlugin";

    @Override
    protected int getDisplayNameResource() {
        return R.string.plugin_name_notification;
    }

    @Override
    protected int getDescriptionResource() {
        return R.string.plugin_description_notification;
    }

    @Override
    public String[] getHandledPacketTypes() {
        return new String[]{NOTIFICATION_PLUGIN_TYPE};
    }

    private Set<String> currentNotifications;
    private Map<String, RepliableNotification> pendingIntents;
    private Map<String, List<Notification.Action>> actions;

    @Override
    public int onCreate() {
        if (!hasPermission()) {
            return 1;
        }

        pendingIntents = new HashMap<>();
        currentNotifications = new HashSet<>();
        actions = new HashMap<>();

        SarahNotificationListenerService.RunCommand(context, new SarahNotificationListenerService.InstanceCallback() {
            @Override
            public void onServiceStart(SarahNotificationListenerService service) {
                service.addListener(NotificationPlugin.this);
            }
        });


        return 0;
    }

    @Override
    public int onDestroy() {

        SarahNotificationListenerService.RunCommand(context, new SarahNotificationListenerService.InstanceCallback() {
            @Override
            public void onServiceStart(SarahNotificationListenerService service) {
                service.removeListener(NotificationPlugin.this);
            }
        });
        return 0;
    }

    @Override
    public int getPluginID() {
        return R.integer.plugin_id_notification;
    }

    @Override
    public void receivedMsg(SarahPacket packet) {

    }

    @Override
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        sendNotificationToDeviceConnection(statusBarNotification);

    }

    private void sendNotificationToDeviceConnection(StatusBarNotification statusBarNotification) {
        Notification notification = statusBarNotification.getNotification();

        if ((notification.flags & Notification.FLAG_FOREGROUND_SERVICE) != 0
                || (notification.flags & Notification.FLAG_ONGOING_EVENT) != 0
//                || (notification.flags & Notification.FLAG_LOCAL_ONLY) != 0
                || (notification.flags & NotificationCompat.FLAG_GROUP_SUMMARY) != 0 //The notification that groups other notifications
        ) {
            //This is not a notification we want!
            return;
        }
        try {
            JSONObject data = new JSONObject();
            SarahPacket packet = new SarahPacket(data);
            data.put("type", NOTIFICATION_PLUGIN_TYPE);
            SarahNotification sarahNotification = new SarahNotification(statusBarNotification);
            data.put("notification", sarahNotification.serializeToJSON(context));
            deviceConnection.sendPacket(packet);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {

    }

    @Override
    public void onListenerConnected(SarahNotificationListenerService service) {

    }

    @Override
    public void onListenerDisconnected(SarahNotificationListenerService service) {

    }

    @Override
    public boolean checkRequiredPermissions() {
        return hasPermission();
    }

    @Override
    public Activity getMainActivity() {
        return null;
    }

    @Override
    public Activity getSettingActivity() {
        return null;
    }

    private boolean hasPermission() {
        String notificationListenerList = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        return (notificationListenerList != null && notificationListenerList.contains(context.getPackageName()));
    }

}
