/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/


package org.gashbeer.sarah.plugins.ClipboardPlugin;

import android.app.Activity;

import org.gashbeer.sarah.R;
import org.gashbeer.sarah.SarahPacket;
import org.gashbeer.sarah.plugins.Plugin;
import org.json.JSONObject;

/**
 * Created by ebrahim on 11/20/19 for project Sarah.
 */
public final class ClipboardPlugin extends Plugin {

    public static String CLIPBOARD_PLUGIN_TYPE = "org.gashbeer.sarah.plugins.clipboard";

    private static final String TAG = "ClipboardPlugin";
    private ClipboardListener.ClipboardObserver observer = new ClipboardListener.ClipboardObserver() {
        @Override
        public void clipboardChanged(String content) {
            try {
                JSONObject object = new JSONObject();
                object.put("type", CLIPBOARD_PLUGIN_TYPE);
                object.put("content", content);
                SarahPacket sarahPacket = new SarahPacket(object);

                deviceConnection.sendPacket(sarahPacket);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected int getDisplayNameResource() {
        return R.string.plugin_name_clipboard;
    }

    @Override
    protected int getDescriptionResource() {
        return R.string.plugin_description_clipboard;
    }

    @Override
    public String[] getHandledPacketTypes() {
        return new String[]{CLIPBOARD_PLUGIN_TYPE};
    }

    @Override
    public int onCreate() {

        ClipboardListener.instance(context).registerObserver(observer);
        return 0;
    }

    @Override
    public int onDestroy() {
        ClipboardListener.instance(context).removeObserver(observer);
        return 0;
    }

    @Override
    public int getPluginID() {
        return R.integer.plugin_id_clipboard;
    }

    @Override
    public Activity getMainActivity() {
        return null;
    }

    @Override
    public Activity getSettingActivity() {
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void receivedMsg(SarahPacket packet) {
        try {
            if (packet.getJsonObject().has("content")) {
                ClipboardListener.instance(context).setText(packet.getJsonObject().getString("content"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
