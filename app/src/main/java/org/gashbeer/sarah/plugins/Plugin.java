/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.plugins;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import org.gashbeer.sarah.connectivity.DeviceConnection;
import org.gashbeer.sarah.connectivity.IMessageHandler;

import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

/**
 * Created by ebrahim on 11/17/19 for project Sarah.
 */
public abstract class Plugin implements IMessageHandler {
    protected Context context;
    protected DeviceConnection deviceConnection;

    public void initPlugin(Context context, DeviceConnection deviceConnection) {
        this.context = context;
        this.deviceConnection = deviceConnection;
    }

    /**
     * this should be set to Plugin's Display Name resource so it can be translated
     *
     * @return Display name's string id
     */
    protected abstract @StringRes
    int getDisplayNameResource();

    /**
     * @return returns translated DisplayName
     */
    public String getDisplayName() {
        return context.getString(getDisplayNameResource());
    }

    /**
     * should be set to Plugin's Description Resource ID
     *
     * @return Description resource ID
     */
    protected abstract @StringRes
    int getDescriptionResource();

    /**
     * @return returns translated Description
     */
    public String getDescription() {
        return context.getString(getDescriptionResource());
    }

    /**
     * @return a string list of packets this plugin can handle
     */
    public abstract String[] getHandledPacketTypes();

    public boolean receiveInvalidPackets() {
        return false;
    }

    /**
     * this method would be called for initializing any listener
     *
     * @return 0 for non-error status
     */
    public abstract int onCreate();

    /**
     * this would be called for destroying any listeners
     *
     * @return 0 for non-error status
     */
    public abstract int onDestroy();

    /**
     * @return a unique ID as the plugin identifier
     */
    public abstract int getPluginID();

    /**
     * @return list of permissions that are required for this plugin to work
     */
    protected String[] getRequiredPermissions() {
        return new String[0];
    }

    /**
     * @return list of Optional permissions that helps plugin behave better
     */
    protected String[] getOptionalPermissions() {
        return new String[0];
    }

    /**
     * Checks if permission is granted
     *
     * @param permission {@link java.security.Permissions}
     * @return true if permission is granted
     */
    protected boolean isPermissionGranted(String permission) {
        int result = ContextCompat.checkSelfPermission(context, permission);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * just like {@link #isPermissionGranted(String)} but this would checks a list of permissions
     *
     * @param permissions a list of system permissions to check {@link java.security.Permissions}
     * @return true if all permissions are granted
     */
    private boolean arePermissionsGranted(String[] permissions) {
        for (String permission : permissions) {
            if (!isPermissionGranted(permission)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkOptionalPermissions() {
        return arePermissionsGranted(getOptionalPermissions());
    }

    public boolean checkRequiredPermissions() {
        return arePermissionsGranted(getRequiredPermissions());
    }

    /**
     * @return Plugin's main Activity if exists otherwise would be null
     */
    public abstract Activity getMainActivity();

    public boolean hasMainActivity(){
        return getMainActivity()!=null;
    }

    /**
     * @return Plugin's Setting Activity if exists otherwise would be null
     */
    public abstract Activity getSettingActivity();

    public boolean hasSettingActivity(){
        return getMainActivity()!=null;
    }
}
