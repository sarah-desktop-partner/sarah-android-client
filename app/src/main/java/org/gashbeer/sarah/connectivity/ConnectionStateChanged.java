/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.connectivity;

/**
 * Created by ebrahim on 11/19/19 for project Sarah.
 */
public interface ConnectionStateChanged {
    void authenticationChanged(DeviceConnection deviceConnection);

    void authenticationGranted(DeviceConnection deviceConnection);

    void authenticationDenied(DeviceConnection deviceConnection);

    void connectionClosed(DeviceConnection deviceConnection);

    void connectionOpened(DeviceConnection deviceConnection);

    void onException(DeviceConnection deviceConnection, Exception e);

}
