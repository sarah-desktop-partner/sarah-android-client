/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.connectivity;

import android.content.Context;
import android.util.Log;

import org.gashbeer.sarah.SarahPacket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by ebrahim on 11/13/19 for project Sarah.
 */
public class BroadcastListenerThread extends Thread {

    private int port;
    private static final String TAG = "UDPClientThread";
    private DatagramSocket sock;
    private IMessageHandler[] messageHandlers;
    private Context context;

    public BroadcastListenerThread(int port, Context context, IMessageHandler... messageHandlers) throws SocketException, UnknownHostException {
        this.port = port;
        this.context = context;
        this.messageHandlers = messageHandlers;
        sock = new DatagramSocket(port, Inet4Address.getByName("0.0.0.0"));
    }

    @Override
    public void run() {
        byte[] bytes = new byte[2048];
        DatagramPacket pckt = new DatagramPacket(bytes, bytes.length);
        while (true) {
            try {
                sock.receive(pckt);

                //Packet received
//                Log.i(TAG, "Packet received from: " + pckt.getAddress().getHostAddress());
                String data = new String(pckt.getData()).trim();
                SarahPacket packet = new SarahPacket(data, pckt.getAddress());
//                Log.i(TAG, "Packet received; data: " + data);
                for (IMessageHandler msgHandler :
                        messageHandlers) {
                    msgHandler.receivedMsg(packet);
                }
            } catch (IOException ioe) {
                Log.e(TAG, "run: " + ioe.getMessage());
            }
        }
    }

}
