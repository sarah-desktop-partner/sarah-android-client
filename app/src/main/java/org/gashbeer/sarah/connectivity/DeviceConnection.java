/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.connectivity;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import org.gashbeer.sarah.Device;
import org.gashbeer.sarah.R;
import org.gashbeer.sarah.SarahNotification;
import org.gashbeer.sarah.SarahPacket;
import org.gashbeer.sarah.SarahPacketQ;
import org.gashbeer.sarah.SarahSocket;
import org.gashbeer.sarah.plugins.Plugin;
import org.gashbeer.sarah.plugins.PluginFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import androidx.annotation.AnyThread;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.RemoteInput;

/**
 * Created by ebrahim on 11/16/19 for project Sarah.
 */
public final class DeviceConnection implements Runnable, SarahSocket.SocketStateChanged {
    private Device device;
    private SarahSocket socket;
    private boolean has_error = false;
    private boolean isAuthenticated = false;
    private boolean isListening = false;
    private String authCode;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    private Context context;
    private static final String TAG = "DeviceConnection";
    private ConnectionStateChanged connectionStateChanged;
    private ConcurrentHashMap<Integer, Plugin> plugins = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, Plugin> pluginsWithoutPermissions = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, Plugin> pluginsWithoutOptionalPermissions = new ConcurrentHashMap<>();
    private SarahPacketQ packetQ;
    private Thread thdPacketQ;

    public DeviceConnection(Context context, Device device, @Nullable ConnectionStateChanged connectionStateChangedListener) {
        this.device = device;
        this.context = context;
        this.connectionStateChanged = connectionStateChangedListener;
        packetQ = new SarahPacketQ(this);

    }

    @Override
    public void run() {
        try {
            socket = new SarahSocket(device.getAddress(), device.getProduct_port(), this);
        } catch (Exception e) {
            has_error = true;
            connectionStateChanged.onException(this, e);
            return;
        }
        try {
            thdPacketQ = new Thread(packetQ);
            thdPacketQ.setName(getDevice().getName() + "," + getDevice().getId() + " : PacketQ");
            thdPacketQ.start();
            if (identify() != 0)
                disconnect();
            if (authenticate() != 0)
                disconnect();
            startReceiving();
        } catch (Exception e) {
            has_error = true;
            disconnect();
        }
    }

    private void startReceiving() {
        byte[] buffer = new byte[2048];
        for (Integer pluginKey :
                PluginFactory.getAvailablePlugins()) {
            addPlugin(pluginKey);
        }
        while (!has_error && isAuthenticated()) {
            isListening = true;
            try {
                int rc = socket.receivePacket(buffer);
                if (rc != -1) {
                    String data = new String(buffer);
                    SarahPacket packet = new SarahPacket(data, socket.getInetAddress());
                    // TODO: 11/20/19 Route to Plugins
                }
            } catch (Exception ex) {
                has_error = true;
                ex.printStackTrace();
            }
            isListening = false;
        }
    }

    public static String getHostName(String defValue) {
        try {

            // TODO: 11/20/19 Get human friendly name
            Method getString = Build.class.getDeclaredMethod("getString", String.class);
            getString.setAccessible(true);
            return getString.invoke(null, "net.hostname").toString();
        } catch (Exception ex) {
            return defValue;
        }
    }

    private void askForCode() {
        Intent intent = new Intent(context, ConnectionService.class);
        intent.setAction(SarahNotification.Channels.Authentication.name);
        intent.putExtra("id", device.getId());
        String replyLabel = context.getResources().getString(R.string.reply_label);
        RemoteInput remoteInput = new RemoteInput.Builder(SarahNotification.Channels.Authentication.intent_key)
                .setLabel(replyLabel)
                .build();
        // Build a PendingIntent for the reply action to trigger.
        PendingIntent replyPendingIntent =
                PendingIntent.getService(context.getApplicationContext(),
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_send,
                        context.getString(R.string.reply_label),
                        replyPendingIntent)
                        .addRemoteInput(remoteInput).build();
        Notification notification =
                new NotificationCompat.Builder(context, SarahNotification.Channels.Authentication.name)
                        .setSmallIcon(R.drawable.logo_foreground_white_small)
                        .setContentTitle(getDevice().getName())
                        .setContentText(context.getString(R.string.insert_auth_key))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setOngoing(false)
                        .setAutoCancel(true)
                        .addAction(action)
                        .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(device.getNotificationID(), notification);
    }

    private JSONObject getIdentity() {
        JSONObject jsonObject = new JSONObject();
//         TODO : Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID)
        String id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (id.isEmpty())
            id = "ef43g34e5ge5ge5";

        StringBuilder OSName = new StringBuilder();
        OSName.append("Android : ").append(Build.VERSION.RELEASE);
        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                OSName.append(" : ").append(fieldName);
            }
        }
        try {
            jsonObject.put("type", "ID");
            jsonObject.put("ID", id);
            jsonObject.put("name", getHostName("Sarah Client"));
            jsonObject.put("os", OSName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public boolean has_error() {
        return has_error;
    }

    public Device getDevice() {
        return device;
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    public boolean isListening() {
        return isListening;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        lock.lock();
        try {
            this.authCode = authCode;
        } finally {
            condition.signal();
            lock.unlock();
        }
    }

    private int identify() {
        try {
            socket.sendPacket(getIdentity());
            byte[] buffer = new byte[2048];
            if (socket.receivePacket(buffer) != -1) {
                String input = new String(buffer);
                JSONObject inp = new JSONObject(input);
                if (inp.getString("type").equals("ID")) {
                    device.setName(inp.getString("name"));
                    device.setOs(inp.getString("os"));
                    device.setProduct_version(inp.getString("version"));
                    Looper.getMainLooper().getThread().setName(device.getName() + " , " + device.getId());
                    return 0;
                }
            }
        } catch (Exception e) {
            return 1;
        }
        return 1;
    }

    private int authenticate() {
        if (isAuthenticated)
            return 0;
        try {
            byte[] buffer = new byte[2048];
            do {
                if (socket.receivePacket(buffer) != -1) {
                    String input = new String(buffer);
                    JSONObject root = new JSONObject(input);
                    if (root.getString("type").equals("Authentication")) {
                        int code = root.getInt("code");
                        if (code != 200) {
                            if (code == 401 && connectionStateChanged != null) {
                                connectionStateChanged.authenticationDenied(this);
                            }
                            if (socket.isClosed())
                                return 1;
                            askForCode();
                            lock.lock();
                            condition.await();
                            JSONObject writeOBJ = new JSONObject();
                            writeOBJ.put("type", "Authentication");
                            writeOBJ.put("key", getAuthCode());
                            lock.unlock();
                            socket.sendPacket(writeOBJ);
                        } else {
                            this.isAuthenticated = true;
                            if (connectionStateChanged != null) {
                                connectionStateChanged.authenticationChanged(this);
                                connectionStateChanged.authenticationGranted(this);
                            }
                            return 0;
                        }
                    }
                } else Log.e(TAG, "authenticate: Disconnected");
            } while (socket.isConnected() && !isAuthenticated);
        } catch (Exception e) {
            has_error = true;
            disconnect();
        }
        return 1;
    }

    private void disconnect() {
        Log.e(TAG, "disconnect: From Server " + device.getName());
        if (socket == null) {
            Log.e(TAG, "disconnect: Socket is null!");
        } else if (socket.isConnected()) {
            try {
                socket.close();
            } catch (IOException e) {
                has_error = true;
                e.printStackTrace();
            } finally {
                onSocketDisconnected(socket);
            }
        }
    }

    public boolean equals(@Nullable DeviceConnection obj) {
        return obj != null && device.equals(obj.getDevice());
    }

    public static Notification getStartConnectionNotification(Context context, Device device) {
        Intent acceptIntent = new Intent(context, ConnectionService.class);
        acceptIntent.setAction(SarahNotification.Channels.Connection.name);
        acceptIntent.putExtra("id", device.getId());
        acceptIntent.putExtra("accept", true);
        Intent declineIntent = new Intent(context, ConnectionService.class);
        declineIntent.setAction(SarahNotification.Channels.Connection.name);
        declineIntent.putExtra("id", device.getId());
        declineIntent.putExtra("accept", false);
        PendingIntent acceptPendingIntent =
                PendingIntent.getService(context.getApplicationContext(),
                        2,
                        acceptIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent declinePendingIntent =
                PendingIntent.getService(context.getApplicationContext(),
                        3,
                        declineIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action acceptAction =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_send,
                        context.getString(R.string.accept),
                        acceptPendingIntent)
                        .build();

        NotificationCompat.Action declineAction =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_send,
                        context.getString(R.string.decline),
                        declinePendingIntent)
                        .build();

        return new NotificationCompat.Builder(context, SarahNotification.Channels.Authentication.name)
                .setSmallIcon(R.drawable.logo_foreground_white_small)
                .setContentTitle(device.getName())
                .setContentText(context.getString(R.string.should_we_connect))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(false)
                .setAutoCancel(true)
                .addAction(acceptAction)
                .addAction(declineAction)
                .build();
    }

    @Override
    protected void finalize() throws Throwable {
        Log.e(TAG, "finalize: we are done");
        super.finalize();
    }

    @Override
    public void onSocketConnected(SarahSocket socket) {
        connectionStateChanged.connectionOpened(this);
    }

    @Override
    public void onSocketDisconnected(SarahSocket socket) {
        has_error = true;
        isAuthenticated = false;
        isListening = false;
        connectionStateChanged.connectionClosed(this);
        Looper.getMainLooper().getThread().interrupt();
    }


    @WorkerThread
    public void sendPacket(byte[] bytes) throws IOException {
        socket.sendPacket(bytes);
    }

    @WorkerThread
    public void sendPacket(String s) throws IOException {
        socket.sendPacket(s);
    }

    @AnyThread
    public void sendPacket(SarahPacket packet) {
        packetQ.addPacket2Q(packet);
    }

    @WorkerThread
    public void sendPacket(JSONObject packet) throws IOException {
        socket.sendPacket(packet);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Plugin Related Functions
    ///////////////////////////////////////////////////////////////////////////

    private synchronized boolean addPlugin(Integer pluginKey) {
        Plugin existing = plugins.get(pluginKey);
        if (existing != null) {
            return true;
        }
        final Plugin plugin = PluginFactory.initiatePluginForDeviceConnection(context, this, pluginKey);
        if (plugin == null) {
            Log.e(TAG, "could not instantiate plugin: " + pluginKey);
            return false;
        }
        boolean success;
        try {
            success = plugin.onCreate() == 0;
        } catch (Exception e) {
            success = false;
            Log.e(TAG, "plugin failed to load " + plugin.getDisplayName(), e);
        }
        plugins.put(pluginKey, plugin);

        if (!plugin.checkRequiredPermissions()) {
            Log.e(TAG, "No permission " + pluginKey);
            plugins.remove(pluginKey);
            pluginsWithoutPermissions.put(pluginKey, plugin);
            success = false;
        } else {
            Log.i(TAG, "Permissions OK " + pluginKey);
            pluginsWithoutPermissions.remove(pluginKey);

            if (plugin.checkOptionalPermissions()) {
                Log.i(TAG, "Optional Permissions OK " + pluginKey);
                pluginsWithoutOptionalPermissions.remove(pluginKey);
            } else {
                Log.e(TAG, "No optional permission " + pluginKey);
                pluginsWithoutOptionalPermissions.put(pluginKey, plugin);
            }
        }
        return success;
    }

    private synchronized boolean removePlugin(Integer pluginKey) {

        Plugin plugin = plugins.remove(pluginKey);

        if (plugin == null) {
            return false;
        }

        try {
            plugin.onDestroy();
            //Log.e("removePlugin","removed " + pluginKey);
        } catch (Exception e) {
            Log.e(TAG, "Exception calling onDestroy for plugin " + pluginKey, e);
        }

        return true;
    }


    public ConcurrentHashMap<Integer, Plugin> getLoadedPlugins() {
        return plugins;
    }

    public ConcurrentHashMap<Integer, Plugin> getPluginsWithoutPermissions() {
        return pluginsWithoutPermissions;
    }

    public ConcurrentHashMap<Integer, Plugin> getPluginsWithoutOptionalPermissions() {
        return pluginsWithoutOptionalPermissions;
    }

}
