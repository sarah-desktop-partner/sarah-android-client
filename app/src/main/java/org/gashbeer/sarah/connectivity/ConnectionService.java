/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah.connectivity;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.gashbeer.sarah.Device;
import org.gashbeer.sarah.DeviceManager;
import org.gashbeer.sarah.R;
import org.gashbeer.sarah.SarahNotification;
import org.gashbeer.sarah.connectivity.BroadcastListenerThread;
import org.gashbeer.sarah.plugins.PluginFactory;
import org.gashbeer.sarah.ui.MainActivity;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import androidx.core.app.NotificationCompat;

public class ConnectionService extends Service {

    private static Thread broadcastReceiverTHD;
    private static final String TAG = "BroadcastReceiverService";

    private static DeviceManager dvManager;
    private final ConnectionBinder binder = new ConnectionBinder();

    public ConnectionService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        dvManager = new DeviceManager(this, statusUpdater);
    }

    public class ConnectionBinder extends Binder {
        public DeviceManager getDeviceManager() {
            return dvManager;
        }

        public void addStatusChangedListener(DeviceManager.ManagementUpdate managementUpdate) {
            dvManager.addStatusChangedListener(managementUpdate);
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intentMain, int flags, int startId) {
        if (intentMain.getAction() == null) {

            startForeground(SarahNotification.Channels.Persistent.id, createForegroundNotification());
            try {
                PluginFactory.initPluginFactory();
                Log.e(TAG, "onPrimaryClipChanged: " + Looper.getMainLooper().getThread().getName());
                if (broadcastReceiverTHD == null) {
                    broadcastReceiverTHD = new BroadcastListenerThread(23519, this, dvManager);
                    broadcastReceiverTHD.setName("UDP Broadcast Receiver");
                }
            } catch (Exception e) {
                Log.e(TAG, "onStartCommand: " + e.getMessage());
                Toast.makeText(getApplicationContext(), "Couldn't start service", Toast.LENGTH_LONG).show();
                return START_NOT_STICKY;
            }
            if (!broadcastReceiverTHD.isAlive())
                broadcastReceiverTHD.start();
        } else {
            if (intentMain.getAction().equals(SarahNotification.Channels.Authentication.name)) {
                Bundle remoteInput = RemoteInput.getResultsFromIntent(intentMain);
                if (remoteInput != null) {
                    String x = remoteInput.getCharSequence(SarahNotification.Channels.Authentication.intent_key).toString();
                    String dvID = intentMain.getExtras().getString("id");
                    if (dvManager.getConFromID(dvID) != null) {
                        dvManager.getConFromID(dvID).setAuthCode(x);
                    }
                    SarahNotification.dismissNotification(this, dvManager.getConFromID(dvID).getDevice().getNotificationID());
                }
            } else if (intentMain.getAction().equals(SarahNotification.Channels.Connection.name)) {
                final String dvID = intentMain.getExtras().getString("id");
                boolean accepted = intentMain.getBooleanExtra("accept", true);
                if (accepted)
                    dvManager.addDevice(dvID);
                List<Device> disconverDevices = dvManager.getDiscoveredDevices();
                Optional<Device> opDv = disconverDevices.stream().filter(new Predicate<Device>() {
                    @Override
                    public boolean test(Device device) {
                        return device.getId().equals(dvID);
                    }
                }).findFirst();
                if (opDv.isPresent())
                    SarahNotification.dismissNotification(this, opDv.get().getNotificationID());

            }
        }
        return START_STICKY;
    }

    //Why is this needed: https://developer.android.com/guide/components/services#Foreground
    private Notification createForegroundNotification() {


        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, SarahNotification.Channels.Persistent.name);
        notification
                .setSmallIcon(R.drawable.logo_foreground_white_small)
                .setOngoing(true)
                .setContentIntent(pi)
                .setPriority(NotificationCompat.PRIORITY_LOW) //MIN so it's not shown in the status bar before Oreo, on Oreo it will be bumped to LOW
                .setShowWhen(false)
                .setAutoCancel(false);

        notification.setGroup("BackgroundService");

        //Pre-oreo, the notification will have an empty title line without this
        notification.setContentTitle(getString(R.string.full_app_name));
        if (!dvManager.anyDeviceConnected())
            notification.setContentText(getString(R.string.foreground_service_no_connection));
        else
            notification.setContentText(getString(R.string.foreground_service_connected) + " " + dvManager.serializeConnectedDevicesToJson());
        return notification.build();
    }

    DeviceManager.ManagementUpdate statusUpdater = new DeviceManager.ManagementUpdate() {
        @Override
        public void onStatusChanged() {

            startForeground(SarahNotification.Channels.Persistent.id, createForegroundNotification());
        }

        @Override
        public void onDiscoveredDevice(Device device) {

        }
    };

}
