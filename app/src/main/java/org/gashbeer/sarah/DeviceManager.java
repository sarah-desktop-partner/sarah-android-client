/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah;

import android.content.Context;
import android.util.Log;

import org.gashbeer.sarah.connectivity.ConnectionStateChanged;
import org.gashbeer.sarah.connectivity.DeviceConnection;
import org.gashbeer.sarah.connectivity.IMessageHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationManagerCompat;

/**
 * Created by ebrahim on 11/15/19 for project Sarah.
 */
public class DeviceManager implements IMessageHandler, ConnectionStateChanged {

    private static final String TAG = "DeviceManager";

    private HashMap<String, DeviceConnection> networkDevices;
    private ArrayList<Device> disconverDevices;
    private Context context;

    private ArrayList<ManagementUpdate> managementUpdate;

    public interface ManagementUpdate {
        void onStatusChanged();

        void onDiscoveredDevice(Device device);
    }


    public DeviceManager(Context context, ManagementUpdate... managementUpdate) {
        this.context = context;
        networkDevices = new HashMap<>();
        disconverDevices = new ArrayList<>();
        this.managementUpdate = new ArrayList<>();
        this.managementUpdate.addAll(Arrays.asList(managementUpdate));
    }


    public void addDevice(final String id) {
        if (networkDevices.get(id) == null) {
            Optional<Device> opDv = disconverDevices.stream().filter(new Predicate<Device>() {
                @Override
                public boolean test(Device device) {
                    return device.getId().equals(id);
                }
            }).findFirst();
            if (opDv.isPresent()) {
                Device dv = opDv.get();
                DeviceConnection deviceConnection = new DeviceConnection(context, dv, this);
                new Thread(deviceConnection).start();
                if (!deviceConnection.has_error()) {
                    networkDevices.put(id, deviceConnection);
                }
            }
        }
    }

    public void removeDevice(Device dv) {
        if (networkDevices.get(dv.getId()) != null) {
            SarahNotification.dismissNotification(context, dv.getNotificationID());
            networkDevices.remove(dv.getId());
            for (ManagementUpdate updater :
                    managementUpdate) {
                updater.onStatusChanged();
            }
        }
    }

    public HashMap<String, DeviceConnection> getNetworkDevices() {
        return networkDevices;
    }

    /**
     * UDP Packet Receiver for Broadcast Receiver
     *
     * @param packet
     */
    @Override
    public void receivedMsg(SarahPacket packet) {
        try {
            if (!packet.hasError() && packet.getJsonObject().has("id")) {

                final String id = packet.getJsonObject().getString("id");
                Optional<Device> opDv = disconverDevices.stream().filter(new Predicate<Device>() {
                    @Override
                    public boolean test(Device device) {
                        return device.getId().equals(id);
                    }
                }).findFirst();
                if (!opDv.isPresent()) {
                    final Device device = new Device(id, packet.getJsonObject().getString("name"), packet.getJsonObject().getString("version"));
                    device.setAddress(packet.getSenderAddress());
                    device.setProduct_port(52599);
                    disconverDevices.add(device);
                    for (ManagementUpdate updater :
                            managementUpdate) {
                        updater.onDiscoveredDevice(device);
                    }
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                    notificationManager.notify(device.getNotificationID(), DeviceConnection.getStartConnectionNotification(context, device));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "UDP receivedMsg: Invalid JSON format!");
        }
    }

    public DeviceConnection getConFromID(String id) {
        return networkDevices.get(id);
    }

    public List<Device> getDiscoveredDevices() {
        return disconverDevices;
    }

    public String serializeConnectedDevicesToJson() {
        final StringBuilder builder = new StringBuilder();
        Iterator<DeviceConnection> i = networkDevices.values().iterator();
        while (i.hasNext()) {
            DeviceConnection deviceConnection = i.next();
            if (deviceConnection.isAuthenticated()) {
                builder.append(deviceConnection.getDevice().getName());
                if (i.hasNext())
                    builder.append(",");
            }
        }
        return builder.toString();
    }

    @NonNull
    @Override
    public String toString() {
        return serializeConnectedDevicesToJson();
    }

    public boolean anyDeviceConnected() {
        boolean isConnected = false;
        Iterator<DeviceConnection> i = networkDevices.values().iterator();
        while (i.hasNext()) {
            DeviceConnection deviceConnection = i.next();
            if (deviceConnection.isAuthenticated())
                isConnected = true;
        }
        return isConnected;
    }

    @Override
    public void authenticationChanged(DeviceConnection deviceConnection) {
        for (ManagementUpdate updater :
                managementUpdate) {
            updater.onStatusChanged();
        }
    }

    @Override
    public void authenticationGranted(DeviceConnection deviceConnection) {
        for (ManagementUpdate updater :
                managementUpdate) {
            updater.onStatusChanged();
        }
    }

    @Override
    public void authenticationDenied(DeviceConnection deviceConnection) {
        for (ManagementUpdate updater :
                managementUpdate) {
            updater.onStatusChanged();
        }
    }

    @Override
    public void connectionClosed(DeviceConnection deviceConnection) {
        removeDevice(deviceConnection.getDevice());
        for (ManagementUpdate updater :
                managementUpdate) {
            updater.onStatusChanged();
        }
        System.gc();
    }

    @Override
    public void connectionOpened(DeviceConnection deviceConnection) {
        for (ManagementUpdate updater :
                managementUpdate) {
            updater.onStatusChanged();
        }
    }

    @Override
    public void onException(DeviceConnection deviceConnection, Exception e) {
        removeDevice(deviceConnection.getDevice());
    }

    public void addStatusChangedListener(ManagementUpdate updater) {
        managementUpdate.add(updater);
    }
}
