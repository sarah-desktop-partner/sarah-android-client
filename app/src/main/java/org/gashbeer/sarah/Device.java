/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah;

import java.net.InetAddress;
import java.util.Random;
import java.util.function.Predicate;

import androidx.annotation.Nullable;

/**
 * Created by ebrahim on 11/14/19 for project Sarah.
 */
public class Device implements Predicate<Device> {
    private String id;
    private String name;
    private String os;
    private String product_version;
    private InetAddress address;
    private int product_port;
    private int notificationID;


    public Device(String id, String name, String product_version) {
        this.id = id;
        this.name = name;
        this.product_version = product_version;
        notificationID = new Random().nextInt();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getProduct_version() {
        return product_version;
    }

    public void setProduct_version(String product_version) {
        this.product_version = product_version;
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress address) {
        this.address = address;
    }

    public int getProduct_port() {
        return product_port;
    }

    public void setProduct_port(int product_port) {
        this.product_port = product_port;
    }

    public boolean equals(@Nullable Device obj) {
        return obj != null && this.getId().equals(obj.getId());
    }

    @Override
    public boolean test(Device device) {
        return device != null && getId().equals(device.getId());
    }

    public int getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(int notificationID) {
        this.notificationID = notificationID;
    }
}
