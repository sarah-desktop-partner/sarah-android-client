/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/


package org.gashbeer.sarah;

import org.gashbeer.sarah.connectivity.DeviceConnection;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by ebrahim on 11/21/19 for project Sarah.
 */
public class SarahPacketQ implements Runnable {

    private Queue<SarahPacket> packets;
    private DeviceConnection connection;
    private Lock qMutex = new ReentrantLock();
    private Condition qCond = qMutex.newCondition();

    public SarahPacketQ(DeviceConnection connection) {
        this.connection = connection;
        packets = new ArrayDeque<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                qMutex.lock();
                if (packets.isEmpty()) {
                    qCond.await();
                }
                SarahPacket packet = packets.poll();
                qMutex.unlock();
                connection.sendPacket(packet.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addPacket2Q(SarahPacket packet) {
        try {
            qMutex.lock();
            packets.add(packet);
        } finally {
            qCond.signal();
            qMutex.unlock();
        }
    }
}
