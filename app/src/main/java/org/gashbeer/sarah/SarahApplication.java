/*************************************************************************
 *    Sarah - Desktop Partner
 *    Android - Client Side
 *    Copyright (C) 2019-2019 Ebrahim Karimi <ebrahimkarimi1378@gmail.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package org.gashbeer.sarah;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

/**
 * Created by ebrahim on 11/13/19 for project Sarah.
 */
public final class SarahApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            initNotificationChannels();
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void initNotificationChannels() {

        NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationChannel persistentChannel = new NotificationChannel(
                SarahNotification.Channels.Persistent.name,
                getApplicationContext().getString(R.string.notification_channel_persistent),
                NotificationManager.IMPORTANCE_LOW);

        manager.createNotificationChannel(persistentChannel);


        NotificationChannel authChannel = new NotificationChannel(
                SarahNotification.Channels.Authentication.name,
                getApplicationContext().getString(R.string.notification_channel_authentication),
                NotificationManager.IMPORTANCE_HIGH);

        manager.createNotificationChannel(authChannel);


    }
}
